from string import *
from Trimmer import trim
import re
class WordPunctTokenizer:
    Reg_count = 0

    def __init__(self, text):
        self.text = text

    def tokenize(text):
        text = trim(text)
        s = re.split('\s+',text)
        return s
    