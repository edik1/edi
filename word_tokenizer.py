from string import *
from Trimmer import trim
import re
class WordTokenizer:

    def __init__(self, text):
        self.text = text

    def tokenize(text):
        text = trim(text)
        s = re.sub("[-0-9ABCDEFGHIJKLMNOPQRSTVWXYZabcdefghijklmnopqrstvwxyz()»,..–]","",text)
        s = re.sub(r'\s+',' ',s)
        s = re.split("[' ',.]",s)
        return s
    